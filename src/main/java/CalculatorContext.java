import java.util.Set;

public class CalculatorContext {
    private Set<OperationStrategy> strategies;

    public CalculatorContext(Set<OperationStrategy> strategies) {
        this.strategies = strategies;
    }

    public int execute(CalculatorInput calcInput) {
        for (OperationStrategy calcValidation : strategies) {
            if (calcValidation.validate(calcInput)) {
                return calcValidation.getResult(calcInput);
            }
        }
        throw new InvalidOperationException("Unsupported calculator operation: " + calcInput.getOperator());
    }
}
