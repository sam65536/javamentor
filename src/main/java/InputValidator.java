public class InputValidator {
    private static String input;
    private static String operator;
    private static int operand1;
    private static int operand2;
    private static boolean resultInRoman;


    public static String getInput() {
        return input;
    }

    public static void setInput(String input) {
        InputValidator.input = input;
    }

    public static String getOperator() {
        return operator;
    }

    public static int getOperand1() {
        return operand1;
    }

    public static int getOperand2() {
        return operand2;
    }

    public static boolean isResultInRoman() {
        return resultInRoman;
    }

    public static boolean isValid() {
        String inputArr[] = input.split("\\s+");

        if (inputArr.length != 3) {
            System.out.println("Invalid expression");
            return false;
        }

        Operand op1 = new Operand(inputArr[0]);
        Operand op2 = new Operand(inputArr[2]);

        if ( op1.isRoman() && !(op2.isRoman()) ) {
            System.out.println("Invalid expression");
            return false;
        }

        if ( !(op1.isRoman()) && op2.isRoman() ) {
            System.out.println("Invalid expression");
            return false;
        }

        if ( op1.isRoman() && op2.isRoman() ) {
            resultInRoman = true;
        }

        operator = inputArr[1];
        operand1 = op1.getValue();
        operand2 = op2.getValue();

        if ( (operand1 < 0) || (operand2 < 0) || (operand1 > 10) || (operand2 > 10) ) {
            System.out.println("Invalid expression");
            return false;
        }

        return true;
    }

    public InputValidator() {
    }
}
