import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        Set<OperationStrategy> calculatorStrategies = new HashSet<>();
        calculatorStrategies.add(CalculatorOperationStrategy.ADD);
        calculatorStrategies.add(CalculatorOperationStrategy.SUBTRACTION);
        calculatorStrategies.add(CalculatorOperationStrategy.MULTIPLICATION);
        calculatorStrategies.add(CalculatorOperationStrategy.DIVISION);
        CalculatorContext ctxCalculator = new CalculatorContext(calculatorStrategies);

        String input = "";
        System.out.print("Enter valid expression: ");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            input = reader.readLine();
        } catch (IOException e) {}

        InputValidator.setInput(input);

        if (InputValidator.isValid()) {
            String operator = InputValidator.getOperator();
            int a = InputValidator.getOperand1();
            int b = InputValidator.getOperand2();

            CalculatorInput calculatorInput = new CalculatorInput(operator, a, b);

            if (InputValidator.isResultInRoman()) {
                System.out.println(RomanNumeralsConverter.arabicToRoman(ctxCalculator.execute(calculatorInput)));
            } else {
                System.out.println(ctxCalculator.execute(calculatorInput));
            }
        }
    }
}
