public class Operand {

    private String literal;

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public boolean isRoman() {
        return this.parseRoman() > 0;
    }

    public Integer getValue() {
        if (isRoman()) return this.parseRoman();
        return this.parseInt();
    }


    private int parseRoman() {
        try {
            return RomanNumeralsConverter.romanToArabic(literal);
        } catch (IllegalArgumentException e) {
            return -1;
        }
    }

    private int parseInt() {
        try {
            return Integer.parseInt(literal);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public Operand(String literal) {
        this.literal = literal;
    }
}

