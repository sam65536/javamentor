import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class CalculatorTest {
    Set<OperationStrategy> calculatorStrategies = new HashSet<>();

    @Before
    public void init() {
        calculatorStrategies.add(CalculatorOperationStrategy.ADD);
        calculatorStrategies.add(CalculatorOperationStrategy.SUBTRACTION);
        calculatorStrategies.add(CalculatorOperationStrategy.MULTIPLICATION);
        calculatorStrategies.add(CalculatorOperationStrategy.DIVISION);
    }

    @Test
    public void testCalculatorOperationForADD() {
        String input = "1 + 2";
        InputValidator.setInput(input);
        Assert.assertTrue(InputValidator.isValid());
        String operator = InputValidator.getOperator();
        CalculatorInput calculatorInput = new CalculatorInput(operator, InputValidator.getOperand1(), InputValidator.getOperand2());
        CalculatorContext ctxCalculator = new CalculatorContext(calculatorStrategies);
        Assert.assertEquals(3, ctxCalculator.execute(calculatorInput));
    }

    @Test
    public void testCalculatorOperationForDIV() {
        String input = "VI / III";
        InputValidator.setInput(input);
        Assert.assertTrue(InputValidator.isValid());
        String operator = InputValidator.getOperator();
        CalculatorInput calculatorInput = new CalculatorInput(operator, InputValidator.getOperand1(), InputValidator.getOperand2());
        CalculatorContext ctxCalculator = new CalculatorContext(calculatorStrategies);
        Assert.assertEquals("II", RomanNumeralsConverter.arabicToRoman(ctxCalculator.execute(calculatorInput)));
    }

    @Test(expected = InvalidOperationException.class)
    public void testCalculatorOperationUnsupported() {
        Set<OperationStrategy> calculatorStrategies = new HashSet<>();
        calculatorStrategies.add(CalculatorOperationStrategy.ADD);
        calculatorStrategies.add(CalculatorOperationStrategy.SUBTRACTION);
        //Now, we decided not to support multiplication
        //calculatorStrategies.add(CalculatorOperation.MULTIPLICATION);
        calculatorStrategies.add(CalculatorOperationStrategy.DIVISION);

        CalculatorInput input = new CalculatorInput("*", 10, 5);
        CalculatorContext ctxCalculator = new CalculatorContext(calculatorStrategies);
        ctxCalculator.execute(input);
    }
}
